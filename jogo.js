const possibilidades = [
	[1,2,3],
	[4,5,6],
	[7,8,9],
	[1,4,7],
	[2,5,8],
	[3,6,9],
	[1,5,9],
	[3,5,7]
];

var vez = 1;
var jogadas = 0;
var fim = false;

const jogador1 = "url(imagens/hacker.jpg)";
const jogador2 = "url(imagens/turma.jpg)";

function jogar(casa){
	if (fim == false){
		quadrado = document.getElementById(casa);
		if (quadrado.style.backgroundImage == "none" || quadrado.style.backgroundImage == ""){
			jogadas++;
			if (vez == 1){
				quadrado.style.backgroundImage = jogador1;
				vez = 2;
				document.getElementById("texto").innerHTML = "Vez dos Cyberamigos";
			}else{
				quadrado.style.backgroundImage = jogador2;
				vez = 1;
				document.getElementById("texto").innerHTML = "Vez do Hacker";
			}
			verificar();
		}
	}else{
		limpar();
	}
}

function limpar(){
	vez = 1;
	jogadas = 0;
	fim = false;
	document.getElementById("texto").innerHTML = "Vez do Hacker";
	for (var i = 1; i <= 9; i++){
		quadrado = document.getElementById(i);
		quadrado.style.backgroundImage = "";
	}
}

function verificar(){
	if (jogadas > 4){
		for (var i = 0; i < possibilidades.length; i++){
			casa1 = document.getElementById(possibilidades[i][0]).style.backgroundImage;
			casa2 = document.getElementById(possibilidades[i][1]).style.backgroundImage;
			casa3 = document.getElementById(possibilidades[i][2]).style.backgroundImage;
			if (casa1 == casa2 && casa1 == casa3 && casa1 !== ""){
				if (vez == 2){
					document.getElementById("texto").innerHTML = " O Hacker ganhou!";
				}else{
					document.getElementById("texto").innerHTML = "Os cyberamigos ganharam!";
				}
				fim = true;
				break;
			}
		}
		if (jogadas > 8 && fim == false){
			document.getElementById("texto").innerHTML = "Deu velha!";
			fim = true;
		}
	}
}